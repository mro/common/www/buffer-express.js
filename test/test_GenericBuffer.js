// @flow
const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),
  { last, first } = require('lodash');

var { GenericBuffer } = require('../src'); /* will start the server */
const maxItems = 5;

describe('GenericBuffer', function() {

  it("can create a generic buffer and manipulate data", () => {
    const buffer = new GenericBuffer("test", { maxItems });

    expect(0, buffer.length);

    buffer.append(0);
    expect(1, buffer.length);

    for (var i = 1; i < 10; i++) {
      buffer.append(i);
    }

    // Check resize
    expect(maxItems, buffer.length);
    expect(6, first(buffer.getBuffer()));
    expect(10, last(buffer.getBuffer()));

    buffer.reset();
    expect(0, buffer.length);
  });

});
