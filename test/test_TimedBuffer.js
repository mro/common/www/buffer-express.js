// @flow
const
  { describe, it } = require('mocha'),
  { expect } = require('chai');

var { TimedBuffer } = require('../src'); /* will start the server */
const retentionMs = 10;

describe('TimedBuffer', function() {
  it("can create a generic buffer and manipulate data", () => {
    const buffer = new TimedBuffer("test", { retentionMs });

    let now = Date.now();

    expect(0, buffer.length);

    buffer.append({ timestamp: now++, field: 1 });
    expect(1, buffer.length);

    for (var i = 1; i < 20; i++) {
      buffer.append({ timestamp: now++, field: 1 });
    }

    // Check resize
    expect(retentionMs, buffer.length);

    buffer.reset();
    expect(0, buffer.length);
  });

});
