
const
  { beforeEach, afterEach } = require('mocha'),
  debug = require('debug')('tests'),
  { makeDeferred } = require('@cern/prom'),
  express = require('express'),
  ews = require('express-ws');

const { noop, attempt } = require('lodash');
const { GenericBuffer, TimedBuffer, BufferEndpoint } = require('../src');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  constructor(maxItems, retentionMs) {
    this.maxItems = maxItems;
    this.retentionMs = retentionMs;
    this.app = express();
    this._prom = this.prepare();
  }

  async prepare() {
    ews(this.app);
    this.router = express.Router();

    this.buffer = new GenericBuffer("generic", { maxItems: this.maxItems });
    this.bufferep = new BufferEndpoint({ rw: true, path: "/gb", buffer: this.buffer });
    this.bufferep.register(this.router);

    this.bufferT = new TimedBuffer("timed", { retentionMs: this.retentionMs });
    this.bufferTep = new BufferEndpoint({ rw: true, path: "/tb", buffer: this.bufferT });
    this.bufferTep.register(this.router);

    BufferEndpoint.registerList(this.router);

    this.app.use("/", this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });
  }

  close() {
    this.bufferep.close();
    this.bufferTep.close();
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(1234, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }
}


module.exports = {
  defaultEnv(maxItems, retentionMs) {
    var env = {};
    let now = Date.now();

    /* app setup */
    beforeEach(function(done) {
      env.server = new Server(maxItems, retentionMs);
      env.server.listen(() => {
        debug(`Server listening on port http://localhost:1234`);
        done();
      });
    });

    /* buffer setup */
    beforeEach(function() {
      for (var i = 1; i < 50; i++) {
        env.server.buffer.append(i);
        env.server.bufferT.append({ timestamp: now++, field: 1 });
      }

      env.updateBuffers = setInterval(() => {
        env.server.buffer.append(1000);
        env.server.bufferT.append({ timestamp: now++, field: 1000 });
      }, 100);
    });

    afterEach(function() {
      clearInterval(env.updateBuffers);
      env.server.close();
      env.server = null;
    });

    return env;
  }
};
