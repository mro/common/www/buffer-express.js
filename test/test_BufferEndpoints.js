// @ts-check
const { delay } = require('@cern/prom');
const
  { describe, it, before, after } = require('mocha'),
  { expect } = require('chai'),
  { assign, last, parseInt } = require('lodash'),
  { Router } = require('express'),
  sa = require('superagent'),
  WebSocket = require('ws'),
  Helpers = require('./Helpers');

const maxItems = 5;
const retentionMs = 10;

describe('BufferEndpoint', function() {

  var env = Helpers.defaultEnv(maxItems, retentionMs);

  describe('GenericBuffer', function() {

    it("can GET", async () => {
      const ret = await sa.get('localhost:1234/gb');
      expect(ret).to.include({ status: 200 });
      expect(ret.body.length).to.equal(maxItems);
    });

    it("can POST", async () => {

      let ret = await sa.post('localhost:1234/gb')
      .send({ data: 1000 });
      expect(ret).to.include({ status: 200 });

      // Get to check the buffer
      ret = await sa.get('localhost:1234/gb');
      expect(ret).to.include({ status: 200 });
      expect(ret.body.length).to.equal(maxItems);
      expect(last(ret.body)).to.equal(1000);
    });

    it("can DELETE", async () => {
      let ret = await sa.delete('localhost:1234/gb');
      expect(ret).to.include({ status: 200 });

      // Get to check the buffer
      ret = await sa.get('localhost:1234/gb');
      expect(ret).to.include({ status: 200 });
      expect(ret.body.length).to.equal(0);
    });

    it("can receive updates via WebSockets", (done) => {
      const wsClient = new WebSocket('ws://localhost:1234/gb');
      let nMessages = 0;
      wsClient.onmessage = (event) => {
        nMessages++;
        if (nMessages === 1) { // First message with the full content of the buffer
          const dataJson = JSON.parse(/** @type string */(event.data));
          expect(dataJson.length).to.equal(maxItems);
        }
        else {
          // @ts-ignore
          expect(parseInt(event.data)).to.equal(1000);
          wsClient.close();
          done();
        }
      };
    });
  });

  describe('TimedBuffer', function() {

    it("can GET", async () => {
      const ret = await sa.get('localhost:1234/tb');
      expect(ret).to.include({ status: 200 });
      expect(ret.body.length).to.equal(retentionMs);
    });

    it("can POST", async () => {
      const now = Date.now();
      let ret = await sa.post('localhost:1234/tb')
      .send({ data: { timestamp: now, field: 1000 } });
      expect(ret).to.include({ status: 200 });

      // Get to check the buffer
      ret = await sa.get('localhost:1234/tb');
      expect(ret).to.include({ status: 200 });
      expect(last(ret.body).field).to.equal(1000);
    });

    it("can DELETE", async () => {
      let ret = await sa.delete('localhost:1234/tb');
      expect(ret).to.include({ status: 200 });

      // Get to check the buffer
      ret = await sa.get('localhost:1234/tb');
      expect(ret).to.include({ status: 200 });
      expect(ret.body.length).to.equal(0);
    });

    it("can receive updates via WebSockets", (done) => {
      const wsClient = new WebSocket('ws://localhost:1234/tb');
      let nMessages = 0;
      wsClient.onmessage = (event) => {
        nMessages++;
        const dataJson = JSON.parse(/** @type string */(event.data));
        if (nMessages === 1) { // First message with the full content of the buffer
          expect(dataJson.length).to.equal(retentionMs);
        }
        else {
          // @ts-ignore
          expect(dataJson.field).to.equal(1000);
          wsClient.close();
          done();
        }
      };
    });
  });
});
