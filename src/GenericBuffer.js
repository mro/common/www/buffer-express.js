// @ts-check
const
  { get, cloneDeep } = require('lodash'),
  { EventEmitter } = require('events');

/**
 * @typedef {any} GenericData
 */

class GenericBuffer extends EventEmitter {
  /**
   * @param {string} name
   * @param {bufferExpress.GenericBuffer.Options?=} options
   */
  constructor(name, options) {
    super();
    this.name = name;
    this.options = options ?? {};
    this.maxItems = get(this.options, 'maxItems', GenericBuffer.DefaultMaxItems);
    /** @type Array<GenericData> */
    this.buffer = [];
  }

  getBuffer() {
    return this.buffer;
  }

  reset() {
    this.buffer = [];
  }

  /**
   * @param {GenericData} data
   */
  append(data) {
    const clonedData = this._preprocessData(cloneDeep(data));
    this.buffer.push(clonedData);
    this._resize();
    this.emit('data', clonedData);
  }

  /**
   * @param {GenericData} data
   */
  _preprocessData(data) {
    return data;
  }

  _resize() {
    // Clean old data
    if (this.buffer.length > this.maxItems) {
      this.buffer.splice(0, this.buffer.length - this.maxItems);
    }
  }
}
GenericBuffer.DefaultMaxItems = 100;

module.exports = GenericBuffer;
