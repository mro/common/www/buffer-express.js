import { expectType } from 'tsd';

import { GenericBuffer, TimedBuffer, BufferEndpoint } from '.';
import express from 'express';

const options: GenericBuffer.Options = {
    maxItems: 10
};

const optionsT: TimedBuffer.Options = {
    retentionMs: 10000
};

const optionsEndPoint: BufferEndpoint.Options = {
    rw: false,
    path: '/buffer-express/test',
};

(async function() {
    const genericBuffer = new GenericBuffer("test", options);
    expectType<string>(genericBuffer.name);
    expectType<GenericBuffer.Options>(genericBuffer.options);

    const genericBufferEp = new BufferEndpoint(optionsEndPoint);
    expectType<GenericBuffer>(genericBufferEp._buffer);
    expectType<BufferEndpoint.Options>(genericBufferEp.options);
    expectType<string>(genericBufferEp.path);

    const timedBuffer = new TimedBuffer("testT", optionsT);
    expectType<string>(timedBuffer.name);
    expectType<TimedBuffer.Options>(timedBuffer.options);

    const timedBufferEp = new BufferEndpoint(optionsEndPoint);

    const app = express();
    genericBufferEp.register(app);
    timedBufferEp.register(app);

    BufferEndpoint.registerList(app);
}());