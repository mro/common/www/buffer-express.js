/// <reference types="node" />

import { NextFunction, Request, Response, Router, RequestHandler } from 'express';
import { EventEmitter } from 'events';

export = bufferExpress;
export as namespace bufferExpress;

declare namespace bufferExpress {

    interface RestOptions {
        rw?: boolean,
        path?: string
    }

    class BufferEndpoint {
        constructor(options?: BufferEndpoint.Options);
        options: BufferEndpoint.Options;
        path: string;
        _buffer: GenericBuffer;

        get(req: Request, res: Response): Promise<any[]>;
        delete(req: Request, res: Response): Promise<void>;
        insert(req: Request, res: Response): Promise<void>;

        register(app: Router): BufferEndpoint;
        static list(req: Request, res: Response): Promise<string[]>;
        static register(app: Router): void;
        close(): void;
        static registerList(app: Router): void;
    }

    namespace BufferEndpoint {
        interface Options extends RestOptions {
            buffer?: GenericBuffer;
            timed?: boolean
        }

        const endpoints: string[];
        const JsonParser: NextFunction;
    }

    namespace ExpressHelpers {
        class HTTPError extends Error {
            constructor(status: number, message: string);
        }
        function wrap(fun: RequestHandler): RequestHandler;
    }

    class GenericBuffer extends EventEmitter {
        constructor(name: string, options?: GenericBuffer.Options | undefined);
        
        name: string;
        options: GenericBuffer.Options;
        maxItems: number;
        buffer: Array<any>

        getBuffer(): Array<any>;
        reset(): void;
        append(data: any): void;
    }

    namespace GenericBuffer {
        interface Options {
            maxItems?: number
        }
    }

    class TimedBuffer extends GenericBuffer {
        constructor(name: string, options?: TimedBuffer.Options | undefined);

        options: TimedBuffer.Options;
        retentionMs: number;
    }

    namespace TimedBuffer {
        interface Options extends GenericBuffer.Options {
            retentionMs?: number
        }
    }
}
