// @ts-check
const
  { get, isNil, last } = require('lodash'),
  GenericBuffer = require('./GenericBuffer');

/**
 * @typedef {import('express').Router} Router
 * @typedef {{timestamp: number} & { [id: string]: any }} TimedData
 */

class TimedBuffer extends GenericBuffer {
  /**
   * @param {string} name
   * @param {bufferExpress.TimedBuffer.Options?=} options
   */
  constructor(name, options) {
    super(name);
    this.options = options ?? {};
    this.retentionMs = get(this.options, 'retentionMs', TimedBuffer.DefaultRetentionMs);
    /** @type {Array<TimedData>} */
    this.buffer = [];
  }

  /**
   * @param {TimedData} data
   */
  _preprocessData(data) {
    if (isNil(data.timestamp)) {
      data.timestamp = Date.now();
    }
    return data;
  }

  _resize() {
    if (this.buffer.length === 0) { return; }
    const lastData = /** @type TimedData */(last(this.buffer));
    const tresholdTime  = lastData.timestamp - this.retentionMs;
    while (this.buffer[0]?.timestamp <= tresholdTime) {
      this.buffer.shift();
    }
  }
}
TimedBuffer.DefaultRetentionMs = 15 * 60 * 1000; // 15 minutes

module.exports = TimedBuffer;
