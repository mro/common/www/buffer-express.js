// @ts-check
const
  debug = require('debug')('rest:buffer'),
  { includes, isNil, get, isArray, forEach } = require('lodash'),
  bodyParser = require('body-parser'),
  TimedBuffer = require('./TimedBuffer'),
  GenericBuffer = require('./GenericBuffer'),
  { HTTPError, wrap } = require('./ExpressHelpers');

/**
 * @typedef {bufferExpress.RestOptions} RestOptions
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 * @typedef {import('express').RequestHandler} RequestHandler
 * @typedef {import('express').Application} App
 * @typedef {import('express').Router} Router
 * @typedef {{ [id: number]: ((data?: Buffer|Error) => void) }} StreamClients
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

const WS_INTERNAL_ERROR = 1011;

var id = 0;
const genId = () => {
  return id++;
};

/**
 * REST wrapper for Buffers
 */
class BufferEndpoint {
  /**
   * BufferEndpoint constructor
   * @param {bufferExpress.BufferEndpoint.Options} options Wrapper options (see details)
   *
   * @details Available options:
   * - buffer: buffer ref provided by the user. It will create a new one if not provided
   * - timed: if buffer is not provided by the user, create a timedBuffer instead of generic one.
   * - path: the HTTP path where endponts are published. Default /buffer-express/{name}
   * - rw: create endpoints that allow to insert or delete buffer content
   *
   */
  constructor(options) {
    this.options = options;

    this._buffer = get(options, 'buffer');
    if (!this._buffer) {
      const timed = get(options, 'timed', false);
      this._buffer = timed ?
        new TimedBuffer(`timed-buffer-${genId()}`) :
        new GenericBuffer(`generic-buffer-${genId()}`);
    }

    this.path = get(this.options, 'path', `/buffer-express/${this._buffer.name}`);

    if (!includes(BufferEndpoint.endpoints, this.path)) {
      BufferEndpoint.endpoints.push(this.path);
    }

    this._id = 0;
    /** @type {StreamClients} */
    this.clients = {};

    this._buffer.on('data', this.broadcast.bind(this));

    debug(`[${this._buffer.name}] Endpoints path:`, this.path);
  }

  close() {
    this._buffer?.removeAllListeners();
    this.clients = {};
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<any[]>}
   */
  async get(req, res) { /* jshint unused:false */
    this._noCache(res);
    const ret = this._buffer ? this._buffer.getBuffer() : null;
    if (isNil(ret)) {
      throw new HTTPError(500);
    }
    return ret;
  }

  /**
   * @param  {Request} req
   * @param  {Response} res
   * @return {Promise<void>}
   */
  async deleteAll(req, res) { /* jshint unused:false */
    this._buffer?.reset();
  }

  /**
   * @brief It expect a body to be like { data: <your data here> }
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  async insert(req, res) { /* jshint unused:false */
    this._noCache(res);
    const data = get(req.body, 'data');
    if (data) {
      this._buffer.append(data);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<string[]>}
   */
  // @ts-ignore
  static async list(req, res) { /* jshint unused:false */
    return BufferEndpoint.endpoints;
  }

  /**
   * @brief disable browser caching on reply
   * @param  {Response} res
   */
  _noCache(res) {
    res.set('Cache-control', 'no-store');
  }

  /**
   * @param {IRouter} router
   * @return {BufferEndpoint}
   */
  register(router) {
    router.get(this.path, wrap(this.get.bind(this)));
    debug(`[${this._buffer.name}] created endpoing GET at ${this.path}`);

    if (router.ws) {
      // @ts-ignore
      router.ws(`${this.path}`, (ws, req) => { /* jshint unused:false */

        const id = this.subscribe((data) => {
          ws.send(JSON.stringify(data), (err) => {
            if (err) { ws.close(WS_INTERNAL_ERROR, err.message); }
          });
        });

        debug(`[${this._buffer.name}] connected id:`, id);

        // First time, send the whole buffer content
        const data = this._buffer?.getBuffer() ?? [];
        ws.send(JSON.stringify(data), (err) => {
          if (err) { ws.close(WS_INTERNAL_ERROR, err.message); }
        });

        ws.once('close', () => {
          debug(`[${this._buffer.name}] disconnected id:`, id);
          this.unsubscribe(id);
        });
      });
      debug(`[${this._buffer.name}] created endpoing WS at ${this.path}`);
    }

    if (this.options.rw) {
      router.post(this.path, BufferEndpoint.JsonParser, wrap(this.insert.bind(this)));
      debug(`[${this._buffer.name}] created endpoing POST at ${this.path}`);
      router.delete(this.path, BufferEndpoint.JsonParser, wrap(this.deleteAll.bind(this)));
      debug(`[${this._buffer.name}] created endpoing DELETE at ${this.path}`);
    }

    return this;
  }

  /**
   * @param {any} data
   */
  broadcast(data) {
    forEach(this.clients, (cb) => cb(data));
  }

  /**
   * @param {(data?: any) => void} cb
   * @return {number}
   */
  subscribe(cb) {
    const id = this._id++;
    this.clients[id] = cb;
    return id;
  }

  /**
   * @param {number} id
   */
  unsubscribe(id) {
    delete this.clients[id];
  }

  /**
   * @param {Router} router
   */
  static registerList(router) {
    const currentRouters = router.stack
    .filter((r) => r.route)    // take out all the middleware
    .map((r) => r.route.path);  // get all the paths
    if (!includes(currentRouters, '/buffer-express/list')) {
      router.get('/buffer-express/list', wrap(BufferEndpoint.list));
    }
  }
}

BufferEndpoint.JsonParser = bodyParser.json({ strict: false });
/** @type {Array<string>} table paths */
BufferEndpoint.endpoints = [];

module.exports = BufferEndpoint;
