// @ts-check
const
  debug = require('debug')('rest:helpers'),
  _ = require('lodash');

/**
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 * @typedef {import('express').RequestHandler} RequestHandler
 */

class HTTPError extends Error {
  /**
   * @param {number} status
   * @param {string=} message
   */
  constructor(status, message) {
    super(message);
    this.status = status;

    switch (Math.floor(status / 100)) {
    case 4:
      this.code = 'HTTPClientError';
      break;
    case 5:
      this.code = 'HTTPServerError';
      break;
    default:
      this.code = 'HTTPError';
      break;
    }
  }
}

/**
 * @param {Response} res
 * @param {any} ret
 * @return {any}
 */
function jsonRet(res, ret) {
  return res.json(_.isNil(ret) ? null : ret);
}

/**
 * @param {Response} res
 * @param {Error} ret
 */
function errorRet(res, ret) {
  debug('error: %o', ret);
  res
  .status(_.get(ret, 'status', 500))
  .send(_.get(ret, 'message', ret));
}

/**
 * @param {(...args: any[]) => any} fun
 * @return {RequestHandler}
 */
function promiseWrapper(fun) {
  return function(req, res, next) {
    try {
      var ret = fun(req, res, next);
      if (_.get(ret, 'then', false)) {
        return ret.then(
          jsonRet.bind(null, res),
          errorRet.bind(null, res));
      }
      jsonRet(res, ret);
      return ret;
    }
    catch (err) {
      errorRet(res, err);
    }
  };
}

module.exports = {
  HTTPError,
  wrap: promiseWrapper
};
