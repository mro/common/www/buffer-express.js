# Provide Buffer utility over HTTP GET and WebSockets

This library implements a buffer and provides endpoints (GET and WebSockets).

## Examples

In your express application:

```javascript
const
  express = require('express'),
  ews = require('express-ws'),
  { GenericBuffer, TimedBuffer, BufferEndpoint } = require('@cern/buffer-express');

class Server {
  constructor() {
    this.app = express();
    this._prom = this.prepare();
  }

  async prepare() {
    ews(this.app);
    this.router = express.Router();

    this.buffer = new GenericBuffer("testGeneric", {});
    this.bufferep = new BufferEndpoint(this.buffer, { rw: true });
    this.bufferep.register(this.router);

    this.bufferT = new TimedBuffer("testTimed", {});
    this.bufferTep = new BufferEndpoint(this.bufferT, { rw: true, path: '/myTimedBuffer' });
    this.bufferTep.register(this.router);

    BufferEndpoint.registerList(this.router);

    this.app.use("/", this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });
  }

  close() {
    this.bufferep.close();
    this.bufferTep.close();
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    var server = this.app.listen(7788, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

}
```

With this example 2 buffers are created and are accesible on the default route `/buffer-express/testGeneric` and `/myTimedBuffer`;

## Tests

To run the unit-tests:

```bash
npm run test
```

## Usage

To use this library as a dependency:

```bash
# Install it
npm install "git+https://gitlab.cern.ch/mro/common/www/buffer-express.js.git"
```
